       IDENTIFICATION DIVISION.
       PROGRAM-ID. trader.
       AUTHOR. TITIMA.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL.
           SELECT TRADER-FILE ASSIGN TO "trader5.dat"
           ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION.
       FD  TRADER-FILE.
       01  TRADER-BUFFER.
           88 END-OF-TRADER-FILE         VALUE HIGH-VALUES.
           05 CITY-ID                    PIC X(2).
           05 PASS                       PIC X(4).
           05 S-COUNT                    PIC 9(6).
       WORKING-STORAGE SECTION.
       01  TOTOL                         PIC X(6) VALUE ZEROS.
       01  RPT-HEADER.
           05 FILLER                     PIC X(8) VALUE "PROVINCE".
           05 FILLER                     PIC X(8) VALUE SPACES .
           05 FILLER                     PIC X(1) VALUE "P".
           05 FILLER                     PIC X(4) VALUE SPACES .
           05 FILLER                     PIC X(6) VALUE "INCOME".
           05 FILLER                     PIC X(6) VALUE SPACES .
           05 FILLER                     PIC X(6) VALUE "MEMBER".
           05 FILLER                     PIC X(4) VALUE SPACES .
           05 FILLER                     PIC X(6) VALUE "INCOME".
       01  RPT-FOOTER.
           05 FILLER                     PIC X(14) 
                                         VALUE "MAX PROVINCE :".

       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT TRADER-FILE
           
           PERFORM READ-TRADER 
           DISPLAY RPT-HEADER

           DISPLAY RPT-FOOTER 
           
           CLOSE TRADER-FILE
           GOBACK .

       READ-TRADER.
           READ TRADER-FILE 
              AT END SET END-OF-TRADER-FILE TO TRUE
           END-READ.
       


